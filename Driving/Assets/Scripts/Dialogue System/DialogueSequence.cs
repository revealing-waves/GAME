using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DialogueSystem
{
    [System.Serializable]
    public class DialogueSequence
    {
        [SerializeField] private string id;
        [SerializeField] private List<DialogueLine> dialogueLines;
        [SerializeField] private List<string> connections;

        #region Properties

        public string ID
        {
            get => id == "" ? "NO ID" : id;
            set => id = value;
        }

        public List<DialogueLine> DialogueLines
        {
            get => dialogueLines;
            set => dialogueLines = value;
        }

        public List<string> Connections
        {
            get => connections;
            set => connections = value;
        }

        #endregion

        

        public DialogueSequence()
        {
            id = "START";
            dialogueLines = new List<DialogueLine>();
            connections = new List<string>();
        }
    }
}

