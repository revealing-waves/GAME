using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DialogueSystem
{
    [System.Serializable]
    public class DialogueLine
    {
        [TextArea(2, 9)] [SerializeField] private string text;

        [Tooltip("Write $PLAYER$ if the player is the speaker")] [SerializeField]
        private string speaker;

        #region Properties

        public string Text
        {
            get => text;
            set => text = value;
        }

        public string Speaker
        {
            get => speaker == "" ? "Player" : speaker;
            set => speaker = value;
        }

        #endregion
        
    }

}
