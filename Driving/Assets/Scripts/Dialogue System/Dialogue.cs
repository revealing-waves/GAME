using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DialogueSystem
{
    [CreateAssetMenu(fileName = "New Dialogue", menuName = "Dialogue", order = 0)]
    public class Dialogue : ScriptableObject
    {
        [SerializeField] private List<DialogueSequence> dialogueNodes = new List<DialogueSequence>();

#if UNITY_EDITOR
        private void Awake()
        {
            if(dialogueNodes.Count == 0) 
                dialogueNodes.Add(defaultNode);
            
        }
#endif
        

        private readonly DialogueSequence defaultNode = new DialogueSequence();

        public IEnumerable<DialogueSequence> GetAllNodes()
        {
            return dialogueNodes;
        }
    }

}
