using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

namespace DialogueSystem.Editor
{
    public class DialogueEditor : EditorWindow
    {
        private const string UNDO_ACTION_TEXT = "Changed Dialogue";
    
        private static Dialogue selectedDialogue;
        
        [MenuItem("Window/Dialogue Editor")]
        public static void Init()
        {
            DialogueEditor editor = GetWindow(typeof(DialogueEditor), false, "Dialogue Editor") as DialogueEditor;
            editor.Show();
        }

        [OnOpenAsset(1)]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            Dialogue openedAsset = EditorUtility.InstanceIDToObject(instanceID) as Dialogue;
            if (openedAsset == null) return false;
            selectedDialogue = openedAsset;
            Init();
            return true;
        }

        private void OnEnable()
        {
            Selection.selectionChanged += UpdateActiveDialogue;
        }

        private void UpdateActiveDialogue()
        {
            Dialogue newDialogue = Selection.activeObject as Dialogue;
            if (newDialogue != null)
            {
                selectedDialogue = newDialogue;
                Repaint();
            }
        }

        private void OnGUI()
        {
            float scaledTime = (float) EditorApplication.timeSinceStartup * 100;
            if (selectedDialogue == null)
            {
                EditorGUILayout.LabelField("No Dialogue Selected!");
            }
            else
            {
                foreach (var node in selectedDialogue.GetAllNodes())
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Sequence");
                    string newID = EditorGUILayout.TextField($"{node.ID}");
                    if (newID != node.ID)
                    {
                        Undo.RecordObject(selectedDialogue, UNDO_ACTION_TEXT);
                        node.ID = newID;
                    }
                    EditorGUILayout.EndHorizontal();
                    
                    foreach (var line in node.DialogueLines)
                    {
                        EditorGUILayout.BeginHorizontal();
                        string newSpeaker = EditorGUILayout.TextField($"{line.Speaker}");
                        if (newSpeaker != line.Speaker)
                        {
                            Undo.RecordObject(selectedDialogue, UNDO_ACTION_TEXT);
                            line.Speaker = newSpeaker;
                        }
                        EditorGUILayout.LabelField("says");
                        string newDialogueText = EditorGUILayout.TextField($"{line.Text}");
                        if (newDialogueText != line.Text)
                        {
                            Undo.RecordObject(selectedDialogue, UNDO_ACTION_TEXT);
                            line.Text = newDialogueText;
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                }
            }
        }

        
        
    }
}

