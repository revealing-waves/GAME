using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Stat : ScriptableObject
{
    [SerializeField] private string statName;
    [TextArea(2,8)][SerializeField] private string description;
    [SerializeField] private FloatVariable floatPointer;
    [SerializeField] private Color color;
    [Space] [SerializeField] private bool clampedValue = false;
    [SerializeField][Tooltip("If clamped value is unchecked, this does nothing.")] private float minValue;
    [SerializeField][Tooltip("If clamped value is unchecked, this does nothing.")] private float maxValue;

    public string StatName
    {
        get => statName;
    }

    public string Description
    {
        get => description;
    }

    public float Value
    {
        get => floatPointer.Value;
        set => floatPointer.Value = clampedValue ? Mathf.Clamp(value, minValue, maxValue) : value;
    }

    public Color Color
    {
        get => color;
    }
}
