using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class OutOfBoundsHandler : MonoBehaviour
{
    private const float RESET_HEIGHT_UNDER = 30.0f;
    [SerializeField] private Transform outOfBoundsUnder;
    private Vector3 initialPosition;

    private void Start()
    {
        initialPosition = transform.position;
    }

    public void CheckOutOfBounds()
    {
        if (outOfBoundsUnder != null)
        {
            if (transform.position.y < initialPosition.y + outOfBoundsUnder.position.y)
            {
                transform.position = initialPosition;
            }

            return;
        }
        if (transform.position.y < initialPosition.y - RESET_HEIGHT_UNDER)
        {
            transform.position = initialPosition;
        }
    }
}
