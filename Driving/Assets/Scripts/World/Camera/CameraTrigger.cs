using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class CameraTrigger : MonoBehaviour
{
    [Tooltip("If blank, I will still track players.")][SerializeField] private GameObject trackedObject;
    [SerializeField] private FollowCamera affectedCamera;
    [SerializeField] private Transform newCameraLocation;
    [SerializeField] private bool makeCameraFollowPlayer;
    [Space] [SerializeField] private bool changeCameraOnCollide = true;
    private void OnTriggerEnter(Collider other)
    {
        if (!changeCameraOnCollide) return;
        if (other.gameObject != trackedObject && other.GetComponent<Interactor>() == null) return;
        AlterCamera();
    }

    public void AlterCamera()
    {
        if(makeCameraFollowPlayer) affectedCamera.SetFollowPlayer(true);
        else affectedCamera.SetFixedPosition(newCameraLocation.position);
    }
}
