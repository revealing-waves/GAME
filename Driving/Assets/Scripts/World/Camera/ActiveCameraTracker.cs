using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveCameraTracker : MonoBehaviour
{
    public static event Action<Camera> OnCameraChange;
    private Camera activeCamera;

    public Camera ActiveCamera
    {
        get => activeCamera;
        private set
        {
            activeCamera = value;
            OnCameraChange?.Invoke(activeCamera);
        }
    }

    private void Awake()
    {
        ActiveCamera = Camera.main;
    }

    public void EnableCamera(Camera newCamera)
    {
        ActiveCamera.gameObject.SetActive(false);
        ActiveCamera = newCamera;
        ActiveCamera.gameObject.SetActive(true);
    }
}
