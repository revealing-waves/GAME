using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class FollowCamera : MonoBehaviour
{
    [HideInInspector] public Camera Camera;
    
    [SerializeField] private Transform toFollow;
    [SerializeField] private float followHeight;
    [SerializeField] private float followDistance;
    [SerializeField] private float heightLookCorrection;
    private Vector3 targetPosition;
    private Quaternion targetRotation;
    [SerializeField] private float smoothTime;
    [SerializeField] private float rotateSmoothing;
    [Space]
    [SerializeField] private bool stationary;

    public bool Stationary => stationary;

    Vector3 velocity = Vector3.zero;

    private void Awake()
    {
        Camera = GetComponent<Camera>();
        targetPosition = transform.position;
    }

    void Update()
    {
        if(!stationary) FollowPlayer();
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
        targetRotation = Quaternion.LookRotation(toFollow.position+heightLookCorrection*Vector3.up-transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation,targetRotation,rotateSmoothing*Time.deltaTime);
    }

    private void FollowPlayer()
    {
        targetPosition = toFollow.TransformPoint(new Vector3(0,followHeight,-followDistance));
    }

    public void SetFixedPosition(Vector3 v)
    {
        stationary = true;
        targetPosition = v;
    }

    public void SetFollowPlayer(bool b)
    {
        stationary = false;
    }
}
