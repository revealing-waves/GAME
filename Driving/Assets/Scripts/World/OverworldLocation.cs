using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverworldLocation : MonoBehaviour
{
    [SerializeField] private OverworldLocationType locationType;
    [SerializeField] private Transform spawnPoint;
    [SerializeField] private Camera spawnCamera;

    public OverworldLocationType LocationType => locationType;
    public Transform SpawnPoint => spawnPoint;
    public Vector3 SpawnPosition => SpawnPoint.position;

    public Camera SpawnCamera => spawnCamera;
}
