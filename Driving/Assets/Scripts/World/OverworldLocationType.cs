using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "World/Location", fileName = "New Location")]
public class OverworldLocationType : ScriptableObject
{
    [SerializeField] private string locationName;
    [TextArea(2,8)][SerializeField] private string description;
    [SerializeField] private Sprite icon;
    [SerializeField] private Color color;
    [SerializeField] private Vector2 gameLocation;

    public string Name => locationName;

    public string Description => description;

    public Sprite Icon => icon;

    public Color Color => color;

    public Vector2 GameLocation => gameLocation;
}
