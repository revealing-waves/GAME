using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorUtility
{
    public static float AngleBetween(Vector3 v, Vector3 u)
    {
        return Mathf.Acos(Vector3.Dot(v.normalized, u.normalized));
    }
}
