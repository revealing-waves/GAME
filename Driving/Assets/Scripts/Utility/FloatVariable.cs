using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class FloatVariable : ScriptableObject, ISerializationCallbackReceiver
{
    public event Action<float> OnVarChange;

    [SerializeField] private float initialValue;
    
    private float _value;
    public float Value
    {
        get => _value;
        set
        {
            _value = value;
            OnVarChange?.Invoke(value);
        }
    }


    public void OnBeforeSerialize()
    {
        
    }

    public void OnAfterDeserialize()
    {
        Value = initialValue;
    }
}
