using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathUtility
{
    public static float InverseLerp(float a, float b, float value)
    {
        return (value - a) / (b - a);
    }
    
    public static float Bounce01(float invar, float midpoint = 0.5f)
    {
        invar %= 1;
        return invar <= midpoint ? InverseLerp(0, midpoint, invar) : InverseLerp(1, midpoint, invar);
    }
    
}
