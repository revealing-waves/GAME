using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Date
{
    public event Action OnDateChange;
    public event Action<int> OnNewDay;
    
    [SerializeField] private int day;
    [SerializeField] private int minute;

    public int Day
    {
        get => day;
        set
        {
            int oldDay = day;
            day = value; 
            OnDateChange?.Invoke();
            if(day > oldDay)
                OnNewDay?.Invoke(value);
        }
    }

    public int Minute
    {
        get => minute;
        set
        {
            minute = (value % 1440 + 1440) % 1440;
            OnDateChange?.Invoke();
        }
    }



    public string FormattedHour
    {
        get
        {
            string first = HHMM[0] < 10 ? $"0{HHMM[0]}" : $"{HHMM[0]}";
            string second = HHMM[1] < 10 ? $"0{HHMM[1]}" : $"{HHMM[1]}";
            return $"{first}:{second}";
        }
    }
    public string FormattedTime => $"Day {Day}, {FormattedHour}";
    public int[] HHMM => new[] {Minute / 60, Minute % 60};

    public void AddTime(int minutes)
    {
        Day += (minute + minutes) / 1440;
        Minute += minutes;
    }

    public void NewDay()
    {
        Day++;
        Minute = 0;
    }

    public void NewDay(int startAtMinute)
    {
        Day++;
        Minute = startAtMinute;
    }

    public void MoveToNext(int toMinute)
    {
        Minute = toMinute;
        if (Minute >= toMinute) Day++;
    }

    public void ResetTime()
    {
        Day = 0;
        Minute = 0;
    }
}
