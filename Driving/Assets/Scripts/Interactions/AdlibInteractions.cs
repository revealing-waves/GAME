using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(InteractibleObject))]
public class AdlibInteractions : MonoBehaviour
{
    [SerializeField] private UnityEvent interactionEvent;

    private InteractibleObject interactible;

    private void Awake()
    {
        interactible = GetComponent<InteractibleObject>();
    }

    private void OnEnable()
    {
        interactible.OnThisInteract += DoAllEvents;
    }

    private void OnDisable()
    {
        interactible.OnThisInteract -= DoAllEvents;
    }

    private void DoAllEvents()
    {
        interactionEvent?.Invoke();
    }
}
