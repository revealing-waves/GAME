using System.Collections;
using System.Collections.Generic;
using GameUI;
using UnityEngine;

[CreateAssetMenu(menuName = "Interactions/Display Panel")]
public class ShowPanel : Interaction
{
    [SerializeField] private InteractionPanel panel;
    private InteractionPanel cachedPanel;

    public override void Interact(Interactor from, InteractibleObject onto)
    {
        Canvas anyCanvas = FindObjectOfType<Canvas>();
        if (anyCanvas == null)
        {
            Debug.LogError("No canvas found in this scene!");
            return;
        }

        if (cachedPanel == null)
        {
            InteractionPanel myPanel = Instantiate(panel, anyCanvas.transform);
            cachedPanel = myPanel;
        }
        else
        {
            cachedPanel.gameObject.SetActive(true);
        }
        cachedPanel.SetInteractor(from);
    }
}
