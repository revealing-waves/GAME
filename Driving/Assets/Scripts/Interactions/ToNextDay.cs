using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Interactions/Start Day")]
public class ToNextDay : Interaction
{
    [SerializeField] private int startAtMinute;
    public override void Interact(Interactor from, InteractibleObject onto)
    {
        onto.Tc.RaiseGoToMinuteEvent(startAtMinute);
    }
}
