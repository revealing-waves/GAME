using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

[CreateAssetMenu(menuName = "Interactions/Stat Changes")]
public class StatsChange : Interaction
{
    [System.Serializable]
    public class StatChange
    {
        public Stat stat;
        public float value;
        [Tooltip("Check true to set value; false to add value")]
        public bool setValue;

        public void DoStatChange()
        {
            stat.Value = setValue ? value : stat.Value + value;
        }
    }

    [SerializeField] private List<StatChange> statChanges;
    public override void Interact(Interactor from, InteractibleObject onto)
    {
        foreach (var changeAction in statChanges)
        {
            changeAction.DoStatChange();
        }

        Debug.Log("Changed Stats.");
    }
}

