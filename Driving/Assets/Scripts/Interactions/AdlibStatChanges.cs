using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

[RequireComponent(typeof(InteractibleObject))]
public class AdlibStatChanges : MonoBehaviour
{

    [System.Serializable]
    public class StatModPack
    {
        public Stat pointer;
        public float value;
        [Tooltip("Sets instead of adds the specified value")] public bool setValue;
    }

    [SerializeField] private List<StatModPack> valuesToModify;
    private InteractibleObject interactible;

    private void Awake()
    {
        interactible = GetComponent<InteractibleObject>();
    }

    private void OnEnable()
    {
        interactible.OnThisInteract += ChangeValues;
    }
    
    private void OnDisable()
    {
        interactible.OnThisInteract -= ChangeValues;
    }

    private void ChangeValues()
    {
        foreach (var smp in valuesToModify)
        {
            smp.pointer.Value = smp.setValue ? smp.value : smp.pointer.Value + smp.value;
        }
    }
}
