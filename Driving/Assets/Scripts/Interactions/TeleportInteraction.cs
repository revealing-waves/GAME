using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Interactions/Teleport")]
public class TeleportInteraction : Interaction
{
    [SerializeField] private OverworldLocationType location;

    public override void Interact(Interactor from = null, InteractibleObject onto = null)
    {
        OverworldTraverser traverser = from.GetComponent<OverworldTraverser>();
        if (traverser == null) return;
        traverser.TeleportTo(location);
    }
}
