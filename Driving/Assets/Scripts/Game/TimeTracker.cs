using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeTracker : MonoBehaviour
{
    public static event Action OnTimeChange;
    public event Action<int> OnDayPass;

    [SerializeField] private Date time;
    [SerializeField] private bool trackingEvents;

    public Date Time => time;

    private void OnEnable()
    {
        Time.OnDateChange += RaiseTimeChangeEvent;
        Time.OnNewDay += RaiseDayPassEvent;
        TimeController.OnTimePass += AddTime;
        TimeController.OnGoToMinute += GoToNext;
    }

    private void OnDisable()
    {
        Time.OnDateChange -= RaiseTimeChangeEvent;
        Time.OnNewDay -= RaiseDayPassEvent;
        TimeController.OnTimePass -= AddTime;
        TimeController.OnGoToMinute -= GoToNext;
    }

    private void RaiseTimeChangeEvent()
    {
        OnTimeChange?.Invoke();
    }

    private void RaiseDayPassEvent(int day)
    {
        if(trackingEvents)
            OnDayPass?.Invoke(day);
    }
    public void AddTime(int toAdd)
    {
        Time.AddTime(toAdd);
    }

    public void GoToNext(int toMinute)
    {
        Time.MoveToNext(toMinute);
    }

    private void OnValidate()
    {
        Time.Day = Time.Day;
        Time.Minute = Time.Minute;
    }
}
