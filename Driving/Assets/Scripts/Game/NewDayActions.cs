using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(TimeTracker))]
public class NewDayActions : MonoBehaviour
{
    private TimeTracker timeTracker;

    [SerializeField] private List<Interaction> actions;
    
    private void Awake()
    {
        timeTracker = GetComponent<TimeTracker>();
    }

    private void OnEnable()
    {
        timeTracker.OnDayPass += DoActions;
    }
    
    private void OnDisable()
    {
        timeTracker.OnDayPass -= DoActions;
    }

    private void DoActions(int day)
    {
        foreach (var a in actions)
        {
            a.Interact();
        }
    }
}
