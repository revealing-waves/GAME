using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ActiveCameraTracker))]
public class OverworldTraversal : MonoBehaviour
{
    [SerializeField] private List<OverworldLocation> overworldLocations;
    private Dictionary<OverworldLocationType, OverworldLocation> worldLocDict;
    private ActiveCameraTracker activeCameraTracker;

    [SerializeField] private OverworldLocation startingLocation;
    private OverworldLocation activeLocation;

    public OverworldLocationType CurrentLocation => activeLocation.LocationType;

    private void Awake()
    {
        activeCameraTracker = GetComponent<ActiveCameraTracker>();
        worldLocDict = new Dictionary<OverworldLocationType, OverworldLocation>();
        foreach (var loc in overworldLocations)
        {
            worldLocDict.Add(loc.LocationType,loc);
        }

        activeLocation = startingLocation;
    }

    private void OnEnable()
    {
        OverworldTraverser.OnTravel += ChangeCameras;
        OverworldTraverser.OnTravel += Derender;
    }
    
    private void OnDisable()
    {
        OverworldTraverser.OnTravel -= ChangeCameras;
        OverworldTraverser.OnTravel -= Derender;
    }

    public OverworldLocation GetWorldLocation(OverworldLocationType type)
    {
        if (worldLocDict.ContainsKey(type))
        {
            return worldLocDict[type];
        }

        return null;
    }

    public static float GetDistanceBetween(OverworldLocationType locA, OverworldLocationType locB)
    {
        return (locA.GameLocation - locB.GameLocation).magnitude;
    }

    private void ChangeCameras(OverworldLocationType location, OverworldTraverser traverser)
    {
        activeCameraTracker.EnableCamera(GetWorldLocation(location).SpawnCamera);
    }

    private void Derender(OverworldLocationType location, OverworldTraverser traverser)
    {
        OverworldLocation worldLoc = GetWorldLocation(location);
        activeLocation.gameObject.SetActive(false);
        worldLoc.gameObject.SetActive(true);
        activeLocation = worldLoc;
    }
}
