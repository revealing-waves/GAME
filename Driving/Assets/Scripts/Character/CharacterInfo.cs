using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Character Info", fileName = "New Character Info")]
public class CharacterInfo : ScriptableObject
{
    [SerializeField] private string charName;
    [SerializeField] private Color color; //placeholder
}
