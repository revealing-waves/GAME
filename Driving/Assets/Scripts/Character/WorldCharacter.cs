using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldCharacter : MonoBehaviour
{
    [SerializeField] private CharacterInfo characterInfo;

    public CharacterInfo CharacterInfo
    {
        get => characterInfo;
        set => characterInfo = value;
    }
}
