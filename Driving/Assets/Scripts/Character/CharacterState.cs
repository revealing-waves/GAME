using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterState : ScriptableObject
{
    public abstract void Act(CharacterStateManager stateManager);
}
