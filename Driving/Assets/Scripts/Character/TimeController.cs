using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

public class TimeController : MonoBehaviour
{
    [SerializeField] private List<FloatVariable> multiplierVariables; //I will only ever need one variable, list is just for fun
    [SerializeField] private float multiplierFactor = 1;

    public int Multiplier =>
        Mathf.CeilToInt(multiplierVariables.Select(x => x.Value).Aggregate(0.0f,(x, y) => f(x) * f(y), res => res) * multiplierFactor);
    public static event Action<int> OnTimePass;
    public static event Action<int> OnGoToMinute;

    public void RaiseTimePassEvent(int timeToPass, bool accountForMultiplier = true)
    {
        int multiplier = accountForMultiplier ? Multiplier : 1;
        OnTimePass?.Invoke(timeToPass * multiplier);
    }

    public void RaiseGoToMinuteEvent(int gotoMin)
    {
        OnGoToMinute?.Invoke(gotoMin);
    }

    
    //The magical function. Edit to modify how multiplier variables scale. I could do strategy pattern, but that feels unnecessary since I'm only
    //ever going to edit this function like maybe 4 times.
    private float f(float x)
    {
        return 1 + 3 * Mathf.Sqrt(x) / Mathf.Sqrt(10);
    }
}
