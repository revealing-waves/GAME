using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverworldTraverser : MonoBehaviour
{
    public static event Action<OverworldLocationType, OverworldTraverser> OnTravel;

    private OverworldTraversal traversalModule;
    [SerializeField] private List<Transform> tagalongs;
    private CharacterController charController;

    public OverworldLocationType CurrentLocation => traversalModule.CurrentLocation;

    private void Awake()
    {
        charController = GetComponent<CharacterController>();
    }

    private void Start()
    {
        traversalModule = FindObjectOfType<OverworldTraversal>();
        if (traversalModule == null)
        {
            Debug.LogWarning("No overworld traversal module found.");
        }
    }

    public void TeleportTo(OverworldLocationType location)
    {
        OverworldLocation worldLocation = traversalModule.GetWorldLocation(location);
        Vector3 vectorToDisplace = worldLocation.SpawnPosition - transform.position;
        foreach (var tag in tagalongs)
        {
            tag.Translate(vectorToDisplace, Space.World);
        }
        if(charController!=null)
            charController.enabled = false;
        transform.Translate(vectorToDisplace, Space.World);
        if(charController!=null)
            charController.enabled = true;
        OnTravel?.Invoke(location, this);
    }
}
