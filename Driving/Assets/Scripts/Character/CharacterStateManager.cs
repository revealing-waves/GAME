using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EntityAction
{
    Interact
}

public enum EntityVectorAction
{
    Move
}


[RequireComponent(typeof(CharacterInfo))]
public class CharacterStateManager : MonoBehaviour
{
    public event Action<EntityVectorAction, Vector2> RaiseVectorAction;
    public event Action<EntityAction> RaiseAction;
    public event Action<bool> RaiseToggleActivate;

    private bool isActive;

    public bool IsActive
    {
        get => isActive;
        set => isActive = value;
    }

    private void Awake()
    {
        isActive = true;
    }
    
    public void ReceiveVectorInput(Vector2 input, EntityVectorAction action)
    {
        if(isActive)
            RaiseVectorAction?.Invoke(action, input);
    }

    public void ReceiveTriggerInput(EntityAction action)
    {
        if(isActive)
            RaiseAction?.Invoke(action);
    }

    public void ToggleActive(bool t)
    {
        isActive = t;
        RaiseToggleActivate?.Invoke(t);
    }
    
}

