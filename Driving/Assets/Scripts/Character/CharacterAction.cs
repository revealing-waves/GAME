using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterStateManager))]
public abstract class CharacterAction : MonoBehaviour
{
    protected CharacterStateManager csm;
    [SerializeField] private bool active;

    public bool Active => csm.IsActive;

    public void SetEntityActionsActive(bool t)
    {
        csm.IsActive = t;
    }

    protected virtual void Awake()
    {
        csm = GetComponent<CharacterStateManager>();
    }
}
