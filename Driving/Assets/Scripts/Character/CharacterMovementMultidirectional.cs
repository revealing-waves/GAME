using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[RequireComponent(typeof(CharacterController), typeof(OutOfBoundsHandler))]
public class CharacterMovementMultidirectional : CharacterAction
{
    private const float NEGLIGIBLE_MOVEMENT = 0.01f;
    
    private CharacterController cc;
    private OutOfBoundsHandler oob;

    private ActiveCameraTracker activeCameraTracker;

    [SerializeField] private float moveSpeed;
    [SerializeField] private float rotateSpeed;
    [SerializeField] private float moveSmoothing;
    [SerializeField] private float gravity;
    [Space] [SerializeField] private Camera relativeCamera;
    private FollowCamera relativeFollowCamera;
    [SerializeField] private bool strafeBackwards;

    private Vector3 targetInputMovementVector;
    private Quaternion targetRotation;
    private Vector3 forceMovementVector;
    private Vector3 currentInputMoveVector;
    private Vector3 moveVector;

    private Vector2 inputVector2;
    protected override void Awake()
    {
        base.Awake();
        cc = GetComponent<CharacterController>();
        oob = GetComponent<OutOfBoundsHandler>();
    }
    private void OnEnable()
    {
        csm.RaiseToggleActivate += KillInputs;
        csm.RaiseVectorAction += Move;
        ActiveCameraTracker.OnCameraChange += ChangeRelativeCamera;
    }

    private void OnDisable()
    {
        csm.RaiseToggleActivate -= KillInputs;
        csm.RaiseVectorAction -= Move;
        ActiveCameraTracker.OnCameraChange -= ChangeRelativeCamera;
    }

    void Start()
    {
        activeCameraTracker = FindObjectOfType<ActiveCameraTracker>();
        targetInputMovementVector = currentInputMoveVector = forceMovementVector = Vector3.zero;
        relativeCamera = activeCameraTracker.ActiveCamera;
        relativeFollowCamera = relativeCamera.GetComponent<FollowCamera>();
    }

    void Update()
    {
        UpdateMovement();
        cc.Move(moveVector * Time.deltaTime);
        UpdateRotation();
        oob.CheckOutOfBounds();
        if(relativeFollowCamera != null)
            strafeBackwards = !relativeFollowCamera.Stationary;
    }
    public void Move(EntityVectorAction action, Vector2 inputVector)
    {
        if (action == EntityVectorAction.Move)
        {
            inputVector2 = inputVector;
            Transform cameraPos = relativeCamera.transform;
            Vector3 camForward = cameraPos.forward;
            Vector2 forward = new Vector2(camForward.x, camForward.z).normalized;
            Vector3 camRight = cameraPos.right;
            Vector2 right = new Vector2(camRight.x, camRight.z).normalized;
            Vector2 input = (inputVector.y * forward + inputVector.x * right).normalized;
            targetInputMovementVector = moveSpeed * new Vector3(input.x, 0, input.y);
        }
    }

    void ApplyGravity()
    {
        forceMovementVector.y -= gravity;
        if (cc.isGrounded)
            forceMovementVector.y = 0;
    }

    void UpdateMovement()
    {
        ApplyGravity();
        currentInputMoveVector = Vector3.Lerp(currentInputMoveVector, targetInputMovementVector, moveSmoothing * Time.deltaTime);
        moveVector = currentInputMoveVector + forceMovementVector;
    }

    void UpdateRotation()
    {
        if (targetInputMovementVector.magnitude < NEGLIGIBLE_MOVEMENT) return;
        Vector3 rotateLookVector = targetInputMovementVector;
        if (strafeBackwards && inputVector2.y < 0)
        {
            rotateLookVector *= -1;
        }
        targetRotation = Quaternion.LookRotation(rotateLookVector, Vector3.up);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotateSpeed);
    }

    void KillInputs(bool b)
    {
        if (b) return;
        targetInputMovementVector = Vector3.zero;
        currentInputMoveVector = Vector3.zero;
    }

    void ChangeRelativeCamera(Camera c)
    {
        relativeCamera = c;
        relativeFollowCamera = c.GetComponent<FollowCamera>();
    }
}
