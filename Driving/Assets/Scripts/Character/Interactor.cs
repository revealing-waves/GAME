using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Interactor : CharacterAction
{
    [SerializeField] private float interactionRadius;
    [Range(0,360)][SerializeField] private float interactionAngle;
    [SerializeField] private LayerMask interactionLayer;
    [SerializeField] private Material interactionHighlightMaterial;

    private InteractibleObject closestInteractible;
    private InteractibleObject lastTouchedInteractible;
    private Material lastTouchedInteractibleMaterial;

    protected override void Awake()
    {
        base.Awake();
    }
    private void OnEnable()
    {
        csm.RaiseAction += TryInteract;
    }

    private void OnDisable()
    {
        csm.RaiseAction -= TryInteract;
        if(lastTouchedInteractible != null)
            lastTouchedInteractible.Render.material = lastTouchedInteractibleMaterial;
    }

    private void Update()
    {
        closestInteractible = FindClosestInteract();
        CheckChangeMaterial();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, interactionRadius);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + Quaternion.AngleAxis(interactionAngle/2, Vector3.up)*transform.forward*interactionRadius);
        Gizmos.DrawLine(transform.position, transform.position + Quaternion.AngleAxis(-interactionAngle/2, Vector3.up)*transform.forward*interactionRadius);
    }

    //Highlight new closest interactible, unhighlight old
    private void CheckChangeMaterial()
    {
        if (closestInteractible == lastTouchedInteractible) return;
        if(lastTouchedInteractible != null)
            lastTouchedInteractible.Render.material = lastTouchedInteractibleMaterial;
        if (closestInteractible != null)
        {
            Renderer closestRenderer = closestInteractible.Render;
            lastTouchedInteractibleMaterial = closestRenderer.material;
            closestRenderer.material = interactionHighlightMaterial;
        }
        lastTouchedInteractible = closestInteractible;
    }
    private InteractibleObject FindClosestInteract()
    {
        Collider[] collisionsBuffer = new Collider[100];
        Physics.OverlapSphereNonAlloc(transform.position, interactionRadius, collisionsBuffer, interactionLayer);
        InteractibleObject closest = null;
        float closestDistance = float.PositiveInfinity;
        foreach (var collision in collisionsBuffer)
        {
            if (collision == null) continue;
            if (collision.GetComponent<InteractibleObject>() == null) continue;
            if (VectorUtility.AngleBetween(collision.transform.position-transform.position, transform.forward) > interactionAngle*Mathf.Deg2Rad/2) 
                continue;
            float distanceTo = Vector3.Distance(collision.transform.position, transform.position);
            if (distanceTo < closestDistance)
            {
                closest = collision.GetComponent<InteractibleObject>();
                closestDistance = distanceTo;
            }
        }
        return closest;
    }

    private void TryInteract(EntityAction action)
    {
        if (action == EntityAction.Interact)
        {
            InteractibleObject interactWith = FindClosestInteract();
            if (interactWith != null)
                interactWith.Interact(this);
        }
    }
}
