using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[RequireComponent(typeof(CharacterController), typeof(OutOfBoundsHandler))]
public class CharacterMovementTank : CharacterAction
{
    private CharacterController cc;
    private OutOfBoundsHandler oob;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float rotateSpeed;
    [SerializeField] private float moveSmoothing;
    [SerializeField] private float rotateSmoothing;
    [SerializeField] private float gravity;
    
    private Vector3 targetInputMovementVector;
    private float currentRotation;
    private float targetRotation;
    private Vector3 forceMovementVector;
    private Vector3 currentInputMoveVector;
    private Vector3 moveVector;

    protected override void Awake()
    {
        base.Awake();
        cc = GetComponent<CharacterController>();
        oob = GetComponent<OutOfBoundsHandler>();
    }

    private void OnEnable()
    {
        csm.RaiseToggleActivate += KillInputs;
        csm.RaiseVectorAction += Move;
    }

    private void OnDisable()
    {
        csm.RaiseToggleActivate -= KillInputs;
        csm.RaiseVectorAction -= Move;
    }

    void Start()
    {
        targetInputMovementVector = currentInputMoveVector = forceMovementVector = Vector3.zero;
    }

    void Update()
    {
        UpdateMovement();
        cc.Move(moveVector * Time.deltaTime);
        transform.Rotate(Time.deltaTime * currentRotation * Vector3.up);
        oob.CheckOutOfBounds();
    }
    public void Move(EntityVectorAction action, Vector2 inputVector)
    {
        if (action == EntityVectorAction.Move)
        {
            targetInputMovementVector = moveSpeed * inputVector.y * transform.forward;
            targetRotation = inputVector.x * rotateSpeed;
        }
    }

    void ApplyGravity()
    {
        forceMovementVector.y -= gravity;
        if (cc.isGrounded)
            forceMovementVector.y = 0;
    }

    void UpdateMovement()
    {
        ApplyGravity();
        currentInputMoveVector = Vector3.Lerp(currentInputMoveVector, targetInputMovementVector, moveSmoothing * Time.deltaTime);
        currentRotation = Mathf.Lerp(currentRotation, targetRotation, rotateSmoothing * Time.deltaTime);
        moveVector = currentInputMoveVector + forceMovementVector;
    }

    void KillInputs(bool b)
    {
        if (b) return;
        targetInputMovementVector = Vector3.zero;
        currentInputMoveVector = Vector3.zero;
        targetRotation = 0;
        currentRotation = 0;
    }
    
}
