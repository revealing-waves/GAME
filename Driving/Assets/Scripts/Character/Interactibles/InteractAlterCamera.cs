using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(InteractibleObject))]
public class InteractAlterCamera : MonoBehaviour
{
    [SerializeField] private CameraTrigger alterationToUse;
    private InteractibleObject interactible;

    private void Awake()
    {
        interactible = GetComponent<InteractibleObject>();
    }

    private void OnEnable()
    {
        interactible.OnThisInteract += Alter;
    }

    private void OnDisable()
    {
        interactible.OnThisInteract -= Alter;
    }

    private void Alter()
    {
        alterationToUse.AlterCamera();
    }
}
