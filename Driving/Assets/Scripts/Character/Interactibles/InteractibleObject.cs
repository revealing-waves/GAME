using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TimeController))]
public class InteractibleObject : MonoBehaviour
{
    public event Action OnThisInteract;
    public static event Action<InteractibleObject> OnInteract;
    
    private TimeController tc;
    [SerializeField] private List<Interaction> interactions;
    [SerializeField] private int minutesPassed;
    [Space]
    [Tooltip("Leave blank if world prop is this object")][SerializeField] private Renderer worldProp;

    public TimeController Tc
    {
        get => tc;
        set => tc = value;
    }

    public Renderer Render
    {
        get => worldProp;
    }

    private void Awake()
    {
        tc = GetComponent<TimeController>();
    }

    private void Start()
    {
        if (worldProp == null)
        {
            worldProp = GetComponent<Renderer>();
        }
    }

    public void Interact(Interactor interactor)
    {
        foreach (var interaction in interactions)
        {
            interaction.Interact(interactor, this);
        }
        tc.RaiseTimePassEvent(minutesPassed);
        OnThisInteract?.Invoke();
        OnInteract?.Invoke(this);
    }

    private void OnValidate()
    {
        minutesPassed = Mathf.Clamp(minutesPassed, 0, 1440);
    }
}
