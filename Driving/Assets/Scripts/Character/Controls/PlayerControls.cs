using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controls
{
    [RequireComponent(typeof(CharacterStateManager))]

    public class PlayerControls : MonoBehaviour
    {
        [SerializeField] private KeyCode InteractKey;
        [SerializeField] private KeyCode UpKey;
        [SerializeField] private KeyCode DownKey;
        [SerializeField] private KeyCode LeftKey;
        [SerializeField] private KeyCode RightKey;

        private CharacterStateManager stateManager;

        void Awake()
        {
            stateManager = GetComponent<CharacterStateManager>();
        }

        void Update()
        {
            Vector2 inputVector = Vector2.zero;
            inputVector.y += Convert.ToInt32(Input.GetKey(UpKey)) - Convert.ToInt32(Input.GetKey(DownKey));
            inputVector.x += Convert.ToInt32(Input.GetKey(RightKey)) - Convert.ToInt32(Input.GetKey(LeftKey));

            stateManager.ReceiveVectorInput(inputVector, EntityVectorAction.Move);
            if (Input.GetKeyDown(InteractKey))
            {
                stateManager.ReceiveTriggerInput(EntityAction.Interact);
            }
        }
    }
}
