using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace GameUI
{
    public class TimeUI : MonoBehaviour
    {
        [SerializeField] private TimeTracker timeToRead;
        [SerializeField] private TextMeshProUGUI textField;
        private void OnEnable()
        {
            UpdateTextField();
            TimeTracker.OnTimeChange += UpdateTextField;
        }
    
        private void OnDisable()
        {
            TimeTracker.OnTimeChange -= UpdateTextField;
        }

        private void UpdateTextField()
        {
            textField.text = timeToRead.Time.FormattedTime;
        }
    }
}
