using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;

namespace GameUI
{
    public class FloatVariableUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI textField;
        [SerializeField] private int rounding = 0;
        [SerializeField] private FloatVariable valueToRead;

        private void Start()
        {
            UpdateTextField(valueToRead.Value);
        }

        private void OnEnable()
        {
            valueToRead.OnVarChange += UpdateTextField;
        }
    
        private void OnDisable()
        {
            valueToRead.OnVarChange -= UpdateTextField;
        }

        private void UpdateTextField(float value)
        {
            float magnitude = Mathf.Pow(10, rounding);
            textField.text = (Mathf.Round(value*magnitude)/magnitude).ToString(CultureInfo.InvariantCulture);
        }
    }
}

