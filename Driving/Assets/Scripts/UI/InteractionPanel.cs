using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(RectTransform))]
public class InteractionPanel : MonoBehaviour
{
    [SerializeField] private bool freezeInteractor;
    private Interactor interactor;
    private RectTransform panel;

    private void Awake()
    {
        panel = GetComponent<RectTransform>();
    }

    private void OnDisable()
    {
        if(interactor!=null)
            ToggleInteractorControls(interactor, true);
        else
        {
            Debug.LogWarning($"Interactor of {this.name} was not recorded for some reason. Controls may still be disabled.");
        }
    }

    public RectTransform Panel => panel;
    public Interactor MyInteractor => interactor;

    public void SetInteractor(Interactor t)
    {
        interactor = t;
        CheckDisableInteractor(t);
    }

    public void CheckDisableInteractor(Interactor t)
    {
        if(freezeInteractor)
            ToggleInteractorControls(t, false);
    }
    
    private void ToggleInteractorControls(Interactor t, bool b)
    {
        t.SetEntityActionsActive(b);
    }
    
}

