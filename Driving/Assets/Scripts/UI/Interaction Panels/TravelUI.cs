using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameUI
{
    public class TravelUI : MonoBehaviour
    {
        [SerializeField] private TravelClient travelClient;

        private void OnEnable()
        {
            TravelButtonUI.OnClick += RequestTravel;
        }

        private void OnDisable()
        {
            TravelButtonUI.OnClick -= RequestTravel;
        }

        private void RequestTravel(TravelButtonUI travelButton)
        {
            travelClient.TeleportTo(travelButton.Location);
        }
    }
}
