using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TravelButtonUI : MonoBehaviour
{
    public static event Action<TravelButtonUI> OnClick;
    
    [SerializeField] private OverworldLocationType toLocation;

    public OverworldLocationType Location => toLocation;

    public void Click()
    {
        OnClick?.Invoke(this);
    }
}
