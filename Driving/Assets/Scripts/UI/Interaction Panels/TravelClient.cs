using System;
using System.Collections;
using System.Collections.Generic;
using GameUI;
using UnityEngine;

[RequireComponent(typeof(InteractionPanel), typeof(TimeController))]
public class TravelClient : MonoBehaviour
{
    [SerializeField] private float distanceTimeScale;

    private TimeController tc;
    private InteractionPanel interactionUI;
    private OverworldTraverser traverser;

    private void Awake()
    {
        interactionUI = GetComponent<InteractionPanel>();
        tc = GetComponent<TimeController>();
    }

    private void Start()
    {
        traverser = interactionUI.MyInteractor.GetComponent<OverworldTraverser>();
        if (traverser == null)
        {
            Debug.LogWarning($"No traverser component found on the interactor {interactionUI.MyInteractor}.");
        }
    }

    public void TeleportTo(OverworldLocationType location, bool closeClient = true, bool passTime = true)
    {
        if (location == null)
        {
            Debug.LogWarning("No location set for this button");
            return;
        }
        
        Vector2 myLocation = traverser.CurrentLocation.GameLocation;
        Vector2 toLocation = location.GameLocation;
        
        traverser.TeleportTo(location);
        
        if (closeClient)
        {
            gameObject.SetActive(false);
        }
        
        if (passTime)
        {
            
            float distanceToTravel = (myLocation - toLocation).magnitude;
            print(myLocation + "..." + toLocation);
            tc.RaiseTimePassEvent(Mathf.RoundToInt(distanceToTravel*distanceTimeScale));
        }
    }
}
